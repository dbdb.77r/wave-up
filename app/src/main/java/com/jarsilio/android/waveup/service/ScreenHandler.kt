/*
 * Copyright (c) 2016-2019 Juan García Basilio
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup.service

import android.app.admin.DevicePolicyManager
import android.content.Context
import android.os.Build
import android.os.PowerManager
import android.os.VibrationEffect
import android.os.Vibrator
import com.jarsilio.android.waveup.extensions.settings
import com.jarsilio.android.waveup.extensions.state
import com.jarsilio.android.waveup.model.SingletonHolder

import eu.chainfire.libsuperuser.Shell
import timber.log.Timber

class ScreenHandler private constructor(context: Context) {
    private val applicationContext: Context = context.applicationContext

    private val powerManager: PowerManager = applicationContext.getSystemService(Context.POWER_SERVICE) as PowerManager
    private val wakeLock: PowerManager.WakeLock = powerManager.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP or PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "waveup:wakelock")
    private val policyManager: DevicePolicyManager = applicationContext.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager

    private var turnOffScreenThread: Thread? = null
    private var turningOffScreen: Boolean = false

    var lastTimeScreenOnOrOff: Long = 0
        private set

    private fun turnOffScreenThread(delay: Long): Thread {
        return object : Thread() {
            override fun run() {
                if (shouldTurnOffScreen()) {
                    Timber.d("Creating a thread to turn off display if still covered in ${delay / 1000.0} seconds")
                    try {
                        vibrateIfSet()
                        Thread.sleep(delay)
                        if (shouldTurnOffScreen()) {
                            vibrateIfSet()
                            doTurnOffScreen()
                        }
                    } catch (e: InterruptedException) {
                        Timber.d("Interrupted thread: Turning off screen cancelled.")
                    }
                }
            }
        }
    }

    private fun shouldTurnOffScreen(): Boolean {
        if (!applicationContext.state.isScreenOn) {
            Timber.v("Should turn off screen? Screen is already off.")
        }
        if (applicationContext.state.isExcludedAppInForeground()) {
            Timber.v("Should turn off screen? Excluded app is running in foreground. Not turning off screen.")
        }
        return applicationContext.state.isScreenOn && !applicationContext.state.isExcludedAppInForeground()
    }

    private fun doTurnOffScreen() {
        turningOffScreen = true // Issue #68. Avoid interrupting the thread if screen is already being turned off.
        lastTimeScreenOnOrOff = System.currentTimeMillis()
        Timber.i("Switched from 'far' to 'near'.")
        if (applicationContext.settings.isLockScreenWithPowerButton) {
            Timber.i("Turning screen off simulating power button press.")
            Shell.SU.run("input keyevent 26")
        } else {
            Timber.i("Turning screen off.")
            try {
                policyManager.lockNow()
            } catch (e: IllegalStateException) {
                Timber.e("Failed to run lockNow() to turn off the screen. Probably due to an ongoing call. Exception: $e")
            } catch (e: SecurityException) {
                Timber.e("Failed to run lockNow() to turn off the screen. Probably due to missing device admin rights, which I don't really understand... Exception: $e")
            }
        }
        turningOffScreen = false
    }

    fun turnOffScreen() {
        turnOffScreenThread = turnOffScreenThread(applicationContext.settings.sensorCoverTimeBeforeLockingScreen)
        turnOffScreenThread?.start()
    }

    fun cancelTurnOff() {
        if (turnOffScreenThread?.state != Thread.State.TERMINATED && !turningOffScreen) {
            Timber.d("Cancelling turning off of display")
            turnOffScreenThread?.interrupt()
        }
    }

    fun turnOnScreen() {
        if (!applicationContext.state.isScreenOn) {
            lastTimeScreenOnOrOff = System.currentTimeMillis()
            Timber.i("Switched from 'near' to 'far'. Turning screen on")
            if (wakeLock.isHeld) {
                wakeLock.release()
            }
            wakeLock.acquire(TIME_SCREEN_ON)
        }
    }

    private fun vibrateIfSet() {
        if (applicationContext.settings.isVibrateWhileLocking) {
            val vibrator = applicationContext.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?

            if (vibrator == null) {
                Timber.e("Failed to access vibrator on device.")
                return
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrator?.vibrate(VibrationEffect.createOneShot(TIME_VIBRATION, VibrationEffect.DEFAULT_AMPLITUDE))
            } else {
                vibrator?.vibrate(TIME_VIBRATION)
            }
        }
    }

    companion object : SingletonHolder<ScreenHandler, Context>(::ScreenHandler) {
        private const val TIME_SCREEN_ON: Long = 5000
        private const val TIME_VIBRATION: Long = 50
    }
}