/*
 * Copyright (c) 2016-2019 Juan García Basilio
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.admin.DevicePolicyManager
import android.content.ActivityNotFoundException
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast

import com.jarsilio.android.privacypolicy.PrivacyPolicyBuilder
import com.jarsilio.android.waveup.extensions.settings
import com.jarsilio.android.waveup.extensions.state
import com.jarsilio.android.waveup.prefs.PreferenceActivity
import com.jarsilio.android.waveup.prefs.Settings
import com.jarsilio.android.waveup.receivers.LockScreenAdminReceiver
import com.jarsilio.android.waveup.service.ProximitySensorHandler
import com.jarsilio.android.waveup.service.WaveUpService
import com.mikepenz.aboutlibraries.Libs
import com.mikepenz.aboutlibraries.LibsBuilder

import eu.chainfire.libsuperuser.Shell
import timber.log.Timber

class MainActivity : PreferenceActivity(), SharedPreferences.OnSharedPreferenceChangeListener {
    private val proximitySensorHandler: ProximitySensorHandler by lazy { ProximitySensorHandler.getInstance(this) }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.d("Starting WaveUp MainActivity (GUI)")

        if (!proximitySensorHandler.isProximitySensorAvailable()) {
            Timber.e("No proximity sensor found!")
            settings.isServiceEnabled = false // Just in case it was enabled at some point in history (which shouldn't be possible from now on)
            showNoProximitySensorAvailableAndFinish()
        } else {
            showInitialDialog()
            WaveUpService.start(this)
            registerPreferencesListener()
            showShowLoveDialog()
        }
    }

    private fun showShowLoveDialog() {
        val twoWeeks = 2 * 7 * 24 * 60 * 60 * 1000
        val now = System.currentTimeMillis()

        if (BuildConfig.FLAVOR == "standard" &&
            settings.showLoveDialog &&
            now - settings.lastLoveDialogShownTimestamp > twoWeeks) {
            settings.lastLoveDialogShownTimestamp = now
            AlertDialog.Builder(this)
                .setTitle(R.string.show_some_love_dialog_title)
                .setMessage(R.string.show_some_love_dialog_text)
                .setPositiveButton(R.string.take_me_there) { dialog, which -> goToGooglePlayStore("com.jarsilio.android.waveup.love") }
                .setNeutralButton(R.string.maybe_later) { dialog, which -> }
                .setNegativeButton(R.string.never_again) { dialog, which -> settings.showLoveDialog = false }
                .setCancelable(false)
                .show()
        }
    }

    private fun goToGooglePlayStore(packageName: String) {
        val uri = Uri.parse("market://details?id=$packageName")
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
            Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
            Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
        try {
            startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            val uri = Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
            startActivity(Intent(Intent.ACTION_VIEW, uri))
        }
    }

    private fun showNoProximitySensorAvailableAndFinish() {
        AlertDialog.Builder(this)
                .setTitle(R.string.missing_proximity_sensor_title)
                .setMessage(R.string.missing_proximity_sensor_text)
                .setPositiveButton(android.R.string.yes, { dialog, which -> finish() })
                .setCancelable(false)
                .show()
    }

    private fun showInitialDialog() {
        if (!settings.isInitialDialogShown) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle(R.string.alert_dialog_title)
            builder.setMessage(R.string.alert_dialog_message)
            builder.setPositiveButton(R.string.alert_dialog_ok_button, null)
            builder.show()

            settings.isInitialDialogShown = true
        }
    }

    private fun showAboutLicensesActivity() {
        LibsBuilder()
                .withActivityStyle(Libs.ActivityStyle.LIGHT_DARK_TOOLBAR)
                .withAboutIconShown(true)
                .withAboutVersionShown(true)
                .withActivityTitle(getString(R.string.licenses_menu_item))
                .withAboutDescription(getString(R.string.licenses_about_libraries_text))
                .start(this)
    }

    private fun showPrivacyPolicyActivity() {
        val privacyPolicyBuilder = PrivacyPolicyBuilder()
                .withIntro(getString(R.string.app_name), "Juan García Basilio (juanitobananas)")
                .withUrl("https://gitlab.com/juanitobananas/wave-up/blob/master/PRIVACY.md#waveup-privacy-policy")
                .withMeSection()
                .withEmailSection("juam+waveup@posteo.net")
                .withAutoGoogleOrFDroidSection()
                .withCustomSection(getString(R.string.privacy_policy_phone_permission))

        privacyPolicyBuilder.start(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        when (id) {
            R.id.privacy_policy_menu_item -> showPrivacyPolicyActivity()
            R.id.revoke_device_admin_menu_item -> removeDeviceAdminPermission()
            R.id.action_usage_menu_item -> startActivity(Intent(android.provider.Settings.ACTION_USAGE_ACCESS_SETTINGS))
            R.id.licenses_menu_item -> showAboutLicensesActivity()
            R.id.impressum_menu_item -> startActivity(Intent(this, ImpressumActivity::class.java))
        }

        return super.onOptionsItemSelected(item)
    }

    @TargetApi(Build.VERSION_CODES.M)
    @SuppressLint("BatteryLife")
    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        proximitySensorHandler.startOrStopListeningDependingOnConditions()
        when (key) {
            Settings.ENABLED -> {
                if (settings.isServiceEnabled) {
                    settings.isPaused = false
                    requestReadPhoneStatePermission()
                }
                WaveUpService.start(this)
                if (settings.isLockScreen && !state.isLockScreenAdmin) {
                    openRequestAdminRightsYesNoDialog()
                }
            }

            Settings.LOCK_SCREEN -> if (settings.isLockScreen && !state.isLockScreenAdmin) {
                openRequestAdminRightsYesNoDialog()
            }

            Settings.LOCK_SCREEN_WITH_POWER_BUTTON -> if (settings.isLockScreenWithPowerButton) {
                settings.isLockScreenWithPowerButton = false
                Thread(Runnable {
                    if (!Shell.SU.available()) {
                        runOnUiThread {
                            Toast.makeText(this, R.string.root_access_failed, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        runOnUiThread {
                            // This is a hack to avoid an infinite loop. Every time we set it to true, this case is executed.
                            unregisterPreferencesListener()
                            settings.isLockScreenWithPowerButton = true
                            registerPreferencesListener()
                        }
                    }
                }).start()
            }

            Settings.SHOW_NOTIFICATION -> {
                if (!settings.isShowNotification && !state.isIgnoringBatteryOptimizations) {
                    Timber.d("Requesting to ignore battery optimizations for WaveUp")
                    startActivityForResult(Intent(android.provider.Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS, Uri.parse("package:$packageName")), BATTERY_OPTIMIZATION_REQUEST_CODE)
                }
                // Need to restart service to add or remove notification
                WaveUpService.restart(this)
            }
        }
    }

    private fun requestReadPhoneStatePermission() {
        val permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, READ_PHONE_STATE_PERMISSION, READ_PHONE_STATE_PERMISSION_REQUEST_CODE)
        }
    }

    private fun openRequestAdminRightsYesNoDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(R.string.lock_admin_rights_explanation)
                .setPositiveButton(android.R.string.yes) { dialog, which -> requestLockScreenAdminRights() }
                .setNegativeButton(android.R.string.no) { dialog, which -> settings.isLockScreen = false }
                .show()
    }

    private fun requestLockScreenAdminRights() {
        val lockScreenAdminComponentName = ComponentName(this, LockScreenAdminReceiver::class.java)
        val intent = Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN)
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, lockScreenAdminComponentName)
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, R.string.lock_admin_rights_explanation)
        startActivityForResult(intent, ADD_DEVICE_ADMIN_REQUEST_CODE)
    }

    private fun removeDeviceAdminPermission() {
        Timber.i("Removing lock screen admin rights")
        val devAdminReceiver = ComponentName(this, LockScreenAdminReceiver::class.java)
        val dpm = this.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        dpm.removeActiveAdmin(devAdminReceiver)

        // If the user cancels the uninstall he/she will have to switch it back on (to request the admin rights again)
        settings.isLockScreen = false
    }

    fun uninstallApp(view: View) {
        // The view parameter is just for the android:onClick attribute to be able to point here.
        if (state.isLockScreenAdmin) {
            removeDeviceAdminPermission()
            removeAdminRights = true
        }

        Timber.i("Uninstalling app")
        val packageURI = Uri.parse("package:$packageName")
        val uninstallIntent = Intent(Intent.ACTION_DELETE, packageURI)
        startActivityForResult(uninstallIntent, UNINSTALL_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            ADD_DEVICE_ADMIN_REQUEST_CODE -> if (!state.isLockScreenAdmin) {
                // If the user does not activate lock admin switch off lock screen option
                settings.isLockScreen = false
            } else {
                proximitySensorHandler.startOrStopListeningDependingOnConditions()
            }
            UNINSTALL_REQUEST_CODE -> if (resultCode == Activity.RESULT_CANCELED && removeAdminRights) {
                val canceledMsg = Toast.makeText(this, R.string.removed_device_admin_rights, Toast.LENGTH_SHORT)
                canceledMsg.show()
                /* Show message UNINSTALL_CANCELED_MSG_SHOW_TIME second */
                object : CountDownTimer(UNINSTALL_CANCELED_MSG_SHOW_TIME.toLong(), UNINSTALL_CANCELED_MSG_SHOW_INTERVAL.toLong()) {
                    override fun onTick(millisUntilFinished: Long) {
                        canceledMsg.show()
                    }

                    override fun onFinish() {
                        canceledMsg.cancel()
                    }
                }.start()
                removeAdminRights = false
            }
            BATTERY_OPTIMIZATION_REQUEST_CODE -> {
                if (!state.isIgnoringBatteryOptimizations) {
                    Timber.d("The user didn't accept the ignoring of the battery optimization. Forcing show_notification to true")
                    settings.isShowNotification = true
                }
                // Need to restart service to add or remove notification
                WaveUpService.restart(this)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        settings.preferenceActivity = this
        registerPreferencesListener()
    }

    override fun onPause() {
        super.onPause()
        settings.preferenceActivity = null
        unregisterPreferencesListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterPreferencesListener()
    }

    private fun registerPreferencesListener() {
        settings.preferences.registerOnSharedPreferenceChangeListener(this)
    }

    private fun unregisterPreferencesListener() {
        settings.preferences.unregisterOnSharedPreferenceChangeListener(this)
    }

    companion object {
        private const val ADD_DEVICE_ADMIN_REQUEST_CODE = 1
        private const val UNINSTALL_REQUEST_CODE = 200
        private const val READ_PHONE_STATE_PERMISSION_REQUEST_CODE = 300
        private const val BATTERY_OPTIMIZATION_REQUEST_CODE = 400
        private const val UNINSTALL_CANCELED_MSG_SHOW_TIME = 5000
        private const val UNINSTALL_CANCELED_MSG_SHOW_INTERVAL = 1000

        private var removeAdminRights = false

        private val READ_PHONE_STATE_PERMISSION = arrayOf("android.permission.READ_PHONE_STATE")
    }
}
