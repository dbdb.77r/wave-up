package com.jarsilio.android.waveup

import android.app.Application
import android.os.Build
import com.squareup.leakcanary.LeakCanary

import timber.log.Timber

const val MAX_TAG_LENGTH = 23

class WaveUpApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(LongTagTree(packageName))
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }
        LeakCanary.install(this)
    }

    inner class LongTagTree(private val packageName: String) : Timber.DebugTree() {

        private fun getMessage(tag: String?, message: String): String {
            return if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                // Tag length limitation (<23): Use truncated package name and add class name to message
                "$tag: $message"
            } else {
                // No tag length limit limitation: Use package name *and* class name
                message
            }
        }

        private fun getTag(tag: String?): String {
            var newTag: String
            if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                // Tag length limitation (<23): Use truncated package name and add class name to message
                newTag = packageName
                if (newTag.length > MAX_TAG_LENGTH) {
                    newTag = "..." + packageName.substring(packageName.length - MAX_TAG_LENGTH + 3, packageName.length)
                }
            } else {
                // No tag length limit limitation: Use package name *and* class name
                newTag = "$packageName ($tag)"
            }

            return newTag
        }

        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
            val newMessage = getMessage(tag, message)
            val newTag = getTag(tag)

            super.log(priority, newTag, newMessage, t)
        }
    }
}
