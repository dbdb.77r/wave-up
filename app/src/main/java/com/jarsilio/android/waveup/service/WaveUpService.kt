/*
 * Copyright (c) 2016-2019 Juan García Basilio
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.jarsilio.android.waveup.MainActivity
import com.jarsilio.android.waveup.R
import com.jarsilio.android.waveup.extensions.isPieOrNewer
import com.jarsilio.android.waveup.extensions.settings
import com.jarsilio.android.waveup.extensions.state

import com.jarsilio.android.waveup.receivers.CallStateReceiver
import com.jarsilio.android.waveup.receivers.OrientationReceiver
import com.jarsilio.android.waveup.receivers.ScreenReceiver

import timber.log.Timber
import com.jarsilio.android.waveup.receivers.HeadsetStateReceiver

class WaveUpService : Service() {
    private val proximitySensorHandler: ProximitySensorHandler by lazy { ProximitySensorHandler.getInstance(this) }
    private val orientationReceiver: OrientationReceiver = OrientationReceiver()
    private val screenReceiver: ScreenReceiver = ScreenReceiver()
    private val headsetStateStateReceiver: HeadsetStateReceiver = HeadsetStateReceiver()

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun start() {
        registerScreenReceiver()
        registerOrientationReceiver()
        registerCallStateReceiver()
        registerHeadsetStateReceiver()
        proximitySensorHandler.startOrStopListeningDependingOnConditions()
    }

    private fun stop() {
        try {
            unregisterReceiver(orientationReceiver)
            unregisterReceiver(screenReceiver)
            unregisterReceiver(headsetStateStateReceiver)
            unregisterCallStateReceiver()
            proximitySensorHandler.stop()
        } catch (e: IllegalArgumentException) {
            Timber.e(e, "Couldn't unregister receiver. It probably wasn't registered. Currently Android offers no way of checking if a register is registered or not. Just catching the IllegalArgumentException and ignoring it")
        }
    }

    override fun onDestroy() {
        Timber.d("onDestroy is being called on WaveUpService")
        super.onDestroy()
        stop()
    }

    override fun onCreate() {
        super.onCreate()
        start()
    }

    private fun registerHeadsetStateReceiver() {
        val filter = IntentFilter(Intent.ACTION_HEADSET_PLUG)
        registerReceiver(headsetStateStateReceiver, filter)
    }

    private fun registerScreenReceiver() {
        val filter = IntentFilter(Intent.ACTION_SCREEN_ON)
        filter.addAction(Intent.ACTION_SCREEN_OFF)
        registerReceiver(screenReceiver, filter)
    }

    private fun registerOrientationReceiver() {
        val filter = IntentFilter(Intent.ACTION_CONFIGURATION_CHANGED)
        registerReceiver(orientationReceiver, filter)
    }

    private fun registerCallStateReceiver() {
        Timber.d("Registering PHONE_STATE receiver (as component)")
        val component = ComponentName(this, CallStateReceiver::class.java)
        packageManager.setComponentEnabledSetting(component, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP)
    }

    private fun unregisterCallStateReceiver() {
        /*
         * For some reason, I actually don't need to actively register this one. It is enough for it
         * to be declared in the manifest. However, I cannot call 'unregisterReceiver' like for the
         * other receivers but I still want to stop receiving the phone intents if the service is
         * deactivated.
         *
         * My source of inspiration: https://stackoverflow.com/questions/6529276/android-how-to-unregister-a-receiver-created-in-the-manifest
         */

        Timber.d("Unregistering PHONE_STATE receiver (as component)")
        val component = ComponentName(this, CallStateReceiver::class.java)
        packageManager.setComponentEnabledSetting(component, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            Timber.d("onStartCommand called")
            val action = intent.action
            if (action != null) {
                Timber.d("Received Start Foreground Intent: %s", action)
                when (action) {
                    RESUME_ACTION -> {
                        Timber.d("Resuming service")
                        settings.isPaused = false
                    }
                    PAUSE_ACTION -> {
                        Timber.d("Pausing service")
                        settings.isPaused = true
                    }
                    DISABLE_ACTION -> {
                        Timber.d("Disabling (completely stopping) service")
                        settings.isPaused = false
                        settings.isServiceEnabled = false
                        stopSelf()
                    }
                }
            }
        } else {
            Timber.e("onStartCommand called with a null Intent. Probably it was killed by the system and it gave us nothing to work with. Starting (or maybe pausing) anyway")
        }

        if (settings.isPaused) {
            stop()
        } else {
            start()
        }

        if (shouldStartForegroundService(this)) {
            startForegroundService()
        }

        return Service.START_STICKY
    }

    private fun startForegroundService() {
        Timber.d("Starting ForegroundService")
        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.action = MAIN_ACTION
        notificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val notificationPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

        val resumeIntent = Intent(this, WaveUpService::class.java)
        resumeIntent.action = RESUME_ACTION
        val resumePendingIntent = PendingIntent.getService(this, 0, resumeIntent, 0)

        val pauseIntent = Intent(this, WaveUpService::class.java)
        pauseIntent.action = PAUSE_ACTION
        val pausePendingIntent = PendingIntent.getService(this, 0, pauseIntent, 0)

        val disableIntent = Intent(this, WaveUpService::class.java)
        disableIntent.action = DISABLE_ACTION
        val disablePendingIntent = PendingIntent.getService(this, 0, disableIntent, PendingIntent.FLAG_CANCEL_CURRENT)

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, "WaveUp persistent notification", NotificationManager.IMPORTANCE_NONE)
            notificationChannel.description = "This notification is used to keep WaveUp alive in the background. You can switch it off if you wish."
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }
        val notificationBuilder = NotificationCompat.Builder(this, "persistent")
                .setContentText(getString(R.string.tap_to_open))
                .setSmallIcon(R.drawable.notification_icon_white)
                .setShowWhen(false)
                .setContentIntent(notificationPendingIntent)
                .setColor(ContextCompat.getColor(this, R.color.colorAccent))
                .setOngoing(true)

        if (settings.isServiceEnabled && !settings.isPaused) {
            notificationBuilder.setContentTitle(getString(R.string.wave_up_service_started))
                    .setTicker(getString(R.string.wave_up_service_started))
        } else {
            notificationBuilder.setContentTitle(getString(R.string.wave_up_service_stopped))
                    .setTicker(getString(R.string.wave_up_service_stopped))
        }

        // We should only reach this code if it *is* enabled so no need to check that
        if (settings.isPaused) {
            notificationBuilder.addAction(0, getString(R.string.resume), resumePendingIntent)
        } else {
            notificationBuilder.addAction(0, getString(R.string.pause), pausePendingIntent)
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notificationBuilder.priority = Notification.PRIORITY_MIN
        }

        notificationBuilder.addAction(0, getString(R.string.disable), disablePendingIntent)
        startForeground(FOREGROUND_ID, notificationBuilder.build())
    }

    companion object {
        /* Notification stuff */

        private const val FOREGROUND_ID = 1001
        private const val MAIN_ACTION = "MAIN_ACTION"
        private const val DISABLE_ACTION = "DISABLE_ACTION"
        private const val RESUME_ACTION = "RESUME_ACTION"
        private const val PAUSE_ACTION = "PAUSE_ACTION"

        const val NOTIFICATION_CHANNEL_ID = "persistent"

        fun shouldStartForegroundService(context: Context): Boolean {
            return !context.state.isIgnoringBatteryOptimizations || context.settings.isShowNotification || context.isPieOrNewer
        }

        fun start(context: Context) {
            val applicationContext = context.applicationContext
            if (applicationContext.settings.isServiceEnabled) {
                Timber.i("Starting WaveUp Service")
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && WaveUpService.shouldStartForegroundService(context)) {
                    applicationContext.startForegroundService(Intent(applicationContext, WaveUpService::class.java))
                } else {
                    applicationContext.startService(Intent(applicationContext, WaveUpService::class.java))
                }
            } else {
                Timber.i("Stopping WaveUp Service")
                applicationContext.stopService(Intent(applicationContext, WaveUpService::class.java))
            }
        }

        fun stop(context: Context) {
            val applicationContext = context.applicationContext
            Timber.i("Stopping WaveUp Service")
            applicationContext.stopService(Intent(applicationContext, WaveUpService::class.java))
        }

        fun restart(context: Context) {
            val applicationContext = context.applicationContext
            applicationContext.settings.isPaused = false
            if (applicationContext.settings.isServiceEnabled) {
                Timber.d("Restarting WaveUp Service")
                stop(applicationContext)
                start(applicationContext)
            }
        }
    }
}
