package com.jarsilio.android.waveup.extensions

import android.content.Context
import android.os.Build
import com.jarsilio.android.waveup.prefs.Settings
import com.jarsilio.android.waveup.service.WaveUpWorldState

val Context.settings: Settings
    get() = Settings.getInstance(this)

val Context.state: WaveUpWorldState
    get() = WaveUpWorldState.getInstance(this)

val Context.isPieOrNewer: Boolean
    get() = android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P
