New in 2.4.3
★ Fixed bug: Show dialog if no proximity sensor is available and avoid crash.

New in 2.4.2
★ Fixed bug on some devices while initializing proximity sensor.

New in 2.4.1
★ Fixed bug introduced in 2.4.0 which wouldn't allow WaveUp to start properly on some Oreo devices.

New in 2.4.0
★ Added Quick Settings Tile
★ Updated Basque translation