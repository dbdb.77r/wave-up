# WaveUp Privacy Policy

This privacy policy governs your use of the software application Scrambled Eggsif for mobile devices that was created by juanitobananas (Juan García Basilio).

## Data collected by me

I, that is, the app developer, do not collect any personal information whatsoever.

## Data collected by Google Play (only applies if downloaded from Google Play Store)

Google Inc. provides the developer, i.e. me, with anonymised user data (aggregate data) through the Google Play Developer Console.

If you review the app Google Inc. will additionally provide information about your device (Device, Manufacturer, Device type, Device language, CPU make, CPU model, Native platform, RAM (MB), Screen size, Screen density (dpi), OpenGL ES version and OS).

Please refer to the [Google Play Store Privacy Policy](https://play.google.com/about/privacy-security) for more information on how Google Inc. handles your data.

## Data collected by F-Droid (only applies if downloaded from F-Droid)

As far as I know, F-Droid Limited does not collect any personal information whatsoever.

Please refer to the [F-Droid web page](https://f-droid.org/about) for more information.

## Email communication

If you contact me per email, I will exclusively use your contact data to process your inquiry.

Please feel free to contact me, e.g. per email ([juam+waveup@posteo.net](mailto:juam+waveup@posteo.net)), if you want me to delete your data.

## Why does WaveUp request 'Phone' permissions? Will WaveUp 'make and manage phone calls'?

No. WaveUp will never make or manage phone calls. It also never knows with whom you speak on the phone. It uses this permission exclusively to know whether there is an ongoing call in order to deactivate itself during the call. No information related to the call is queried nor stored. Also the fact that there is an ongoing call is only known by the WaveUp app and is never sent elsewhere.
