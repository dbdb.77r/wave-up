# FAQ

## WaveUp stops working after some time on my MIUI device

MIUI uses a task manager and a battery manager. Just add WaveUp to the whitelists:

- `Security` → `Permissions` → `Autostart` and select `WaveUp`
- `Settings` → `Battery & performance` → `Manager apps battery usage` → `Choose apps` → `WaveUp` and select `No restriction`.

This might be useful for newer devices:
- `Settings` → `Apps` → `Settings` → `Special access` → `Ignore optimisations` → `WaveUp` and select `Allow` (from http://nine-faq.9folders.com/articles/16876-how-to-turn-off-battery-optimization-on-the-huawei-devices).

# WaveUp stops working after some time on my device (Android Marshmallow or newer)

Try disabling the notification in WaveUp. This will trigger a request for disabling battery optimization for WaveUp. Accept it and see if it works better.

